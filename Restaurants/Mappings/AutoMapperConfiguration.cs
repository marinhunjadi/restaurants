﻿using ApplicationModel.Models;
using AutoMapper;
using Restaurants.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace Restaurants.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<City, CityViewModel>();
                cfg.CreateMap<Restaurant, RestaurantViewModel>();
                cfg.CreateMap<RestaurantFormViewModel, Restaurant>()
                    .ForMember(r => r.Name, map => map.MapFrom(vm => vm.Name))
                    //.ForMember(r => r.Location.Latitude, map => map.MapFrom(vm => vm.Latitude))
                    //.ForMember(r => r.Location.Longitude, map => map.MapFrom(vm => vm.Longitude))
                    //.ForMember(r => r.Location, map => map.MapFrom(vm => DbGeography.FromText(vm.Location)))
                    .ForMember(r => r.CityID, map => map.MapFrom(vm => vm.CityID));
            });
        }
    }
}