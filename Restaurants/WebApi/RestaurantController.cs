﻿using ApplicationData;
using ApplicationModel.Models;
using ApplicationService;
using AutoMapper;
using Restaurants.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace RestaurantsTestApplication.Controllers
{
    //[Authorize]
    public class RestaurantController : ApiController
    {
        private readonly IRestaurantService restaurantService;

        public RestaurantController(IRestaurantService restaurantService)
        {
            this.restaurantService = restaurantService;
        }

        // GET api/restaurant
        public IEnumerable<RestaurantViewModel> Get()
        {
            IEnumerable<Restaurant> restaurants;
            IEnumerable<RestaurantViewModel> viewModelRestaurants;

            restaurants = restaurantService.GetRestaurants().ToList();

            viewModelRestaurants = Mapper.Map<IEnumerable<Restaurant>, IEnumerable<RestaurantViewModel>>(restaurants);
            return viewModelRestaurants;
        }

        // GET api/restaurant
        public IEnumerable<RestaurantViewModel> Get(int cityID, int start, int length)
        {
            IEnumerable<Restaurant> restaurants;
            IEnumerable<RestaurantViewModel> viewModelRestaurants;

            restaurants = restaurantService.GetRestaurantsByCityId(cityID, start, length);

            viewModelRestaurants = Mapper.Map<IEnumerable<Restaurant>, IEnumerable<RestaurantViewModel>>(restaurants);
            return viewModelRestaurants;
        }

        // GET api/restaurant/5
        [ResponseType(typeof(ArrayList))]
        public IHttpActionResult Get(string id)
        {
            Restaurant restaurant;
            RestaurantViewModel viewModelRestaurant;
            ArrayList list = new ArrayList();

            restaurant = restaurantService.GetRestaurant(Int32.Parse(id));

            if (restaurant == null)
            {
                return NotFound();
            }

            ApplicationEntities context = new ApplicationEntities();
            var idParam = new SqlParameter
            {
                ParameterName = "RestaurantId",
                Value = Int32.Parse(id)
            };
            var nearestRestaurants = context.Database.SqlQuery<RestaurantViewModel>("EXEC uspGetNearestRestaurants @RestaurantId", idParam).ToList<RestaurantViewModel>();

            viewModelRestaurant = Mapper.Map<Restaurant, RestaurantViewModel>(restaurant);
            list.Add(viewModelRestaurant);
            list.Add(nearestRestaurants);
            return Ok(list);
        }

        // POST api/restaurant
        [ResponseType(typeof(Restaurant))]
        public IHttpActionResult Post(RestaurantFormViewModel formViewModelRestaurant)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Restaurant restaurant = new Restaurant();
            restaurant.Name = formViewModelRestaurant.Name;
            restaurant.Location = DbGeography.FromText(formViewModelRestaurant.Location);
            restaurant.CityID = formViewModelRestaurant.CityID;
            //Restaurant restaurant;
            //restaurant = Mapper.Map<RestaurantFormViewModel, Restaurant>(formViewModelRestaurant);

            try
            {
                restaurantService.CreateRestaurant(restaurant);
                restaurantService.SaveRestaurant();
            }
            catch (Exception)
            {
                throw;
            }

            return CreatedAtRoute("DefaultApi", new { id = restaurant.RestaurantID }, restaurant);
        }
    }
}
