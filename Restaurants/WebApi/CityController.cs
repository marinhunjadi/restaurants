﻿using ApplicationModel.Models;
using ApplicationService;
using AutoMapper;
using Restaurants.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace RestaurantsTestApplication.Controllers
{
    //[Authorize]
    public class CityController : ApiController
    {
        private readonly ICityService cityService;

        public CityController(ICityService cityService)
        {
            this.cityService = cityService;
        }

        // GET api/city
        public IEnumerable<CityViewModel> Get()
        {
            IEnumerable<City> cities;
            IEnumerable<CityViewModel> viewModelCities;

            cities = cityService.GetCities().ToList();

            viewModelCities = Mapper.Map<IEnumerable<City>, IEnumerable<CityViewModel>>(cities);
            return viewModelCities;
        }

        // GET api/city/5
        [ResponseType(typeof(CityViewModel))]
        public IHttpActionResult Get(string id)
        {
            City city;
            CityViewModel viewModelCity;

            city= cityService.GetCity(Int32.Parse(id));

            if (city == null)
            {
                return NotFound();
            }

            viewModelCity = Mapper.Map<City, CityViewModel>(city);
            return Ok(viewModelCity);
        }
    }
}
