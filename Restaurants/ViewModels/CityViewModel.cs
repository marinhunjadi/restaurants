﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Restaurants.ViewModels
{
    public class CityViewModel
    {
        public int CityID { get; set; }
        public string Name { get; set; }

        public List<RestaurantViewModel> Restaurants { get; set; }
    }
}