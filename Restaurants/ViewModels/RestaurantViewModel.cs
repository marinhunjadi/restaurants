﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace Restaurants.ViewModels
{
    public class RestaurantViewModel
    {
        public int RestaurantID { get; set; }
        public string Name { get; set; }
        //public string Location { get; set; }
        public double Distance { get; set; }

        public int CityID { get; set; }
    }
}