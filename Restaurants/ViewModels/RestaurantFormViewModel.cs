﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Restaurants.ViewModels
{
    public class RestaurantFormViewModel
    {
        public int RestaurantID { get; set; }
        public string Name { get; set; }
        //public DbGeography Location { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Location { get; set; }

        public int CityID { get; set; }
    }
}