﻿using ApplicationData.Infrastrucure;
using ApplicationData.Repositories;
using ApplicationModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationService
{
    // operations you want to expose
    public interface IRestaurantService
    {
        IEnumerable<Restaurant> GetRestaurants();
        IEnumerable<Restaurant> GetCityRestaurants(string cityName, string restaurantName = null);
        IEnumerable<Restaurant> GetRestaurantsByCityId(int cityID, int start, int length);
        Restaurant GetRestaurant(int id);
        void CreateRestaurant(Restaurant restaurant);
        void SaveRestaurant();
    }

    public class RestaurantService : IRestaurantService
    {
        private readonly IRestaurantRepository restaurantsRepository;
        private readonly ICityRepository cityRepository;
        private readonly IUnitOfWork unitOfWork;

        public RestaurantService(IRestaurantRepository restaurantsRepository, ICityRepository cityRepository, IUnitOfWork unitOfWork)
        {
            this.restaurantsRepository = restaurantsRepository;
            this.cityRepository = cityRepository;
            this.unitOfWork = unitOfWork;
        }

        #region IRestaurantService Members

        public IEnumerable<Restaurant> GetRestaurants()
        {
            var restaurants = restaurantsRepository.GetAll();
            return restaurants;
        }

        public IEnumerable<Restaurant> GetCityRestaurants(string cityName, string restaurantName = null)
        {
            var city = cityRepository.GetCityByName(cityName);
            return city.Restaurants.Where(g => g.Name.ToLower().Contains(restaurantName.ToLower().Trim()));
        }

        public IEnumerable<Restaurant> GetRestaurantsByCityId(int cityID, int start, int length)
        {
            return restaurantsRepository.GetRestaurantsByCityId(cityID, start, length);
        }

        public Restaurant GetRestaurant(int id)
        {
            var restaurant = restaurantsRepository.GetById(id);
            return restaurant;
        }

        public void CreateRestaurant(Restaurant restaurant)
        {
            restaurantsRepository.Add(restaurant);
        }

        public void SaveRestaurant()
        {
            unitOfWork.Commit();
        }

        #endregion
    }
}
