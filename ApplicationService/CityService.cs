﻿using ApplicationData.Infrastrucure;
using ApplicationData.Repositories;
using ApplicationModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationService
{
    // operations you want to expose
    public interface ICityService
    {
        IEnumerable<City> GetCities(string name = null);
        City GetCity(int id);
        City GetCity(string name);
        void CreateCity(City city);
        void SaveCity();
    }

    public class CityService : ICityService
    {
        private readonly ICityRepository cityRepository;
        private readonly IUnitOfWork unitOfWork;

        public CityService(ICityRepository cityRepository, IUnitOfWork unitOfWork)
        {
            this.cityRepository = cityRepository;
            this.unitOfWork = unitOfWork;
        }

        #region ICityService Members

        public IEnumerable<City> GetCities(string name = null)
        {
            if (string.IsNullOrEmpty(name))
                return cityRepository.GetAll();
            else
                return cityRepository.GetAll().Where(c => c.Name == name);
        }

        public City GetCity(int id)
        {
            var city = cityRepository.GetById(id);
            return city;
        }

        public City GetCity(string name)
        {
            var city = cityRepository.GetCityByName(name);
            return city;
        }

        public void CreateCity(City city)
        {
            cityRepository.Add(city);
        }

        public void SaveCity()
        {
            unitOfWork.Commit();
        }

        #endregion
    }
}
