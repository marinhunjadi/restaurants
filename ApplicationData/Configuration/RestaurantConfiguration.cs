﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
//using System.Data.Spatial;
using ApplicationModel.Models;

namespace ApplicationData.Configuration
{
    public class RestaurantConfiguration : EntityTypeConfiguration<Restaurant>
    {
        public RestaurantConfiguration()
        {
            ToTable("Restaurants");
            Property(r => r.Name).IsRequired().HasMaxLength(100);            
            Property(r => r.CityID).IsRequired();
        }
    }
}
