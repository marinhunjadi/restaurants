﻿using ApplicationData.Configuration;
using ApplicationModel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationData
{
    public class ApplicationEntities : DbContext
    {
        public ApplicationEntities() : base("DefaultConnection") { }

        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<City> Cities { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new RestaurantConfiguration());
            modelBuilder.Configurations.Add(new CityConfiguration());
        }
    }
}
