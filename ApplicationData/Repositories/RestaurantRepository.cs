﻿using ApplicationData.Infrastrucure;
using ApplicationModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationData.Repositories
{
    public class RestaurantRepository : RepositoryBase<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public IEnumerable<Restaurant> GetRestaurantsByCityId(int cityID, int start, int length)
        {
            var restaurants = this.DbContext.Restaurants;

            if(cityID > 0)
                return restaurants.Where(r => r.CityID == cityID).OrderBy(r => r.Name).Skip(start).Take(length).ToList();
            else
                return restaurants.OrderBy(r => r.Name).Skip(start).Take(length).ToList();
        }
    }

    public interface IRestaurantRepository : IRepository<Restaurant>
    {
        IEnumerable<Restaurant> GetRestaurantsByCityId(int cityID, int start, int length);
    }
}
