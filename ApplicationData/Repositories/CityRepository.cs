﻿using ApplicationData.Infrastrucure;
using ApplicationModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationData.Repositories
{
    public class CityRepository : RepositoryBase<City>, ICityRepository
    {
        public CityRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public City GetCityByName(string cityName)
        {
            var category = this.DbContext.Cities.Where(c => c.Name == cityName).FirstOrDefault();

            return category;
        }

        /*public override void Update(City entity)
        {
            entity.DateUpdated = DateTime.Now;
            base.Update(entity);
        }*/
    }

    public interface ICityRepository : IRepository<City>
    {
        City GetCityByName(string cityName);
    }
}
