﻿using ApplicationModel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationData
{
    public class ApplicationSeedData : DropCreateDatabaseIfModelChanges<ApplicationEntities>
    {
        protected override void Seed(ApplicationEntities context)
        {
            GetCities().ForEach(c => context.Cities.Add(c));
            GetRestaurants().ForEach(r => context.Restaurants.Add(r));
            context.Commit();
            string query = @"
                CREATE PROCEDURE uspGetNearestRestaurants
                    @RestaurantId int
                AS
                    SET NOCOUNT ON;
                    DECLARE @current GEOGRAPHY;
					SET @current = (SELECT Location FROM Restaurants WHERE RestaurantId = @RestaurantId);
                    SELECT *, Location.STDistance(@current) AS Distance FROM Restaurants
                    WHERE RestaurantId != @RestaurantId AND CityId = 
                    (SELECT CityID FROM Restaurants WHERE RestaurantId = @RestaurantId)
                    ORDER BY Location.STDistance(@current);";
            context.Database.ExecuteSqlCommand(query);            
        }

        private static List<City> GetCities()
        {
            return new List<City>
            {
                new City {
                    Name = "Zagreb"
                },
                new City {
                    Name = "Split"
                },
                new City {
                    Name = "Rijeka"
                },
                new City {
                    Name = "Pula"
                },
                new City {
                    Name = "Dubrovnik"
                }
            };
        }

        private static List<Restaurant> GetRestaurants()
        {
            return new List<Restaurant>
            {
                new Restaurant {
                    Name = "Restoran Kvatrić",
                    Location = DbGeography.FromText("POINT(45.815709 15.998259)"),
                    CityID = 1
                },
                new Restaurant {
                    Name = "Restoran stari Puntijar",
                    Location = DbGeography.FromText("POINT(45.850527 15.975085)"),
                    CityID = 1
                },
                new Restaurant {
                    Name = "Didov San",
                    Location = DbGeography.FromText("POINT(45.817558 15.973212)"),
                    CityID = 1
                },
                new Restaurant {
                    Name = "Pri Zvoncu",
                    Location = DbGeography.FromText("POINT(45.795279 15.968924)"),
                    CityID = 1
                },
                new Restaurant {
                    Name = "Stari fijaker Restoran 900",
                    Location = DbGeography.FromText("POINT(45.813967 15.970693)"),
                    CityID = 1
                },
                new Restaurant {
                    Name = "Takenoko",
                    Location = DbGeography.FromText("POINT(45.810916 15.972272)"),
                    CityID = 1
                }
            };
        }
    }
}
